﻿using System;

[Serializable]
public class GamePreferencesData {
    public string _playerName;
    public string _teamName;
    public int _score;
    public float _musicVolume;
    public float _SFXVolume;
}